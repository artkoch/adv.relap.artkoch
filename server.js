var express = require('express');
var path = require('path');
var webpack = require('webpack');
var config = require(__dirname + '/webpack.config.js');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');

var app = express();

var compiler = webpack(config);

app.use(webpackDevMiddleware(compiler, {noInfo: true, publicPath: config.output.publicPath}));
app.use(webpackHotMiddleware(compiler));

app.use(express.static(__dirname + '/'));
app.use(express.static(__dirname + '/client/download'));

app.get('/', function (req, res) {
  res.sendFile(path.resolve( __dirname + '/index.html'));
})


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
})
