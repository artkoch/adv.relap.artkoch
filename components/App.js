import React, {Component, PropTypes} from 'react';
import FirstScreen from './FirstScreen.jsx';
import SecondScreen from './SecondScreen.jsx';
import ThirdScreen from './ThirdScreen.jsx';
import ForthScreen from './ForthScreen.jsx';
import FifthScreen from './FifthScreen.jsx';
import NavigationBar from './NavigationBar.jsx';
import css from '../client/stylus/style.styl';

var Scroll = require('react-scroll');
var Element = Scroll.Element;

class App extends React.Component {
	render() {
		return (
			<div className='wrapper'>
				<NavigationBar />
				<FirstScreen />
				<Element name="secondScreen">
					<SecondScreen />
				</Element>
				<Element name="thirdScreen">
					<ThirdScreen />
				</Element>
				<Element name="forthScreen">
					<ForthScreen />
				</Element>
				<Element name="fifthScreen">
					<FifthScreen />
				</Element>
			</div>
		);
	}
}

export default (App)
