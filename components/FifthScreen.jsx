import React, { Component } from 'react'
import ContactFormRules from "./ContactFormRules.jsx"
import ContactForm from "./ContactForm.jsx"
import Validation from 'react-validation'

class FifthScreen extends React.Component {
	render(props){
		return (
			<div className="screen screen-fifth" id="screen-fifth">
				<div className="screen-fifth__header">
					<h3 className="h3">Напишите нам и&nbsp;запустите<br/>рекламную кампанию <br/>в&nbsp;лучших российских СМИ</h3>
				</div>
				<ContactForm />
				<div className="screen-fifth__footer-links">
					<ul className="ul ul--links">
						<li className="li li--links footer-links__li"><a className="link link--footer" href="tel:+7(965)327-82-85">+7 (965) 327-82-85</a></li>
						<li className="li li--links footer-links__li"><a className="link link--footer" href="mailto:ad@relap.io">ad@relap.io</a></li>
						<li className="li li--links footer-links__li"><a className="link link--footer" href="https://blog.relap.ru">Блог</a></li>
						<li className="li li--links footer-links__li"><a className="link link--footer" href="http://help.relap.info/collection/1-faq">FAQ</a></li>
						<li className="li li--links footer-links__li"><a className="link link--footer link--footer-facebook" href="https://www.facebook.com/Relapio-1396388014013891/"  target="_blank"><svg className="facebook-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5.88 11.32"><title>facebook-icon</title><g id="Layer_2" data-name="Layer 2"><g id="All"><path id="f" d="M3.81,11.32V6.16H5.55l.26-2h-2V2.86c0-.58.16-1,1-1H5.88V.08A14.26,14.26,0,0,0,4.32,0,2.42,2.42,0,0,0,1.74,2.66V4.14H0v2H1.74v5.16Z"/></g></g></svg></a></li>
					</ul>
				</div>
			</div>
		)
	}
}

export default FifthScreen
