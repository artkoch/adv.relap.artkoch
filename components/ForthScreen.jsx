import React, { Component } from 'react'

import KeyPage from 'babel!svg-react!./svg/icon/KeyPage.svg?name=KeyPage';
import LikePage from 'babel!svg-react!./svg/icon/LikePage.svg?name=LikePage';
import PagesPage from 'babel!svg-react!./svg/icon/PagesPage.svg?name=PagesPage';

class ForthScreen extends React.Component {
	render(){
		return (
			<div className="screen screen-forth" id="screen-forth">
				<div className="trigger-types">
					<div className="trigger-types__list-item">
						<div className="trigger-types__icons">
							<PagesPage className="trigger-types__icon trigger-types__icons1"/>
						</div>
						<div className="trigger-types__body">
							<h3 className="h3">Площадки</h3>
							<p className="p trigger-types--text">Мы&nbsp;белая рекламная сеть и&nbsp;можем предоставить возможность выбора площадок для анонсирования.</p>
						</div>
					</div>
					<div className="trigger-types__list-item">
						<div className="trigger-types__icons">
							<LikePage className="trigger-types__icon trigger-types__icons2"/>
						</div>
						<div className="trigger-types__body">
							<h3 className="h3">Интересы</h3>
							<p className="p trigger-types--text">На&nbsp;основе тех материалов, которые читает пользователь мы&nbsp;узнаем его интересы и&nbsp;показываем максимально релевантные объявления.</p>
						</div>
					</div>
					<div className="trigger-types__list-item">
						<div className="trigger-types__icons">
							<KeyPage className="trigger-types__icon trigger-types__icons3"/>
						</div>
						<div className="trigger-types__body">
							<h3 className="h3">Ключевые слова</h3>
							<p className="p trigger-types--text">Наши роботы анализирует контент статей и&nbsp;мы&nbsp;имеем возможность показывать видео в&nbsp;материалах, которые содержат определенные слова.</p>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default ForthScreen
