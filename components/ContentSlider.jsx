import React, { Component } from 'react';

import InlineVideoWidget from 'babel!svg-react!./svg/icon/InlineVideoWidget.svg?name=InlineVideoWidget';
import InlineWidget from 'babel!svg-react!./svg/icon/InlineWidget.svg?name=InlineWidget';
import PostArticleWidget from 'babel!svg-react!./svg/icon/PostArticleWidget.svg?name=PostArticleWidget';
import SideWidget from 'babel!svg-react!./svg/icon/SideWidget.svg?name=SideWidget';
import TosterWidget from 'babel!svg-react!./svg/icon/TosterWidget.svg?name=TosterWidget';
import InteractiveWidget from 'babel!svg-react!./svg/icon/InteractiveWidget.svg?name=InteractiveWidget';


import "../node_modules/react-image-gallery/styles/css/image-gallery.css";

import ImageGallery from 'react-image-gallery';

class ContentSlider extends React.Component {
  constructor(props){
    super(props);
  }
  handleImageLoad(event) {
    console.log('Image loaded ', event.target)
  }
  componentDidMount() {
    document.body.style.overflow = "hidden";
    document.getElementById('nav-bg').scrollTop = 0;
  }

  componentWillUnmount() {
    document.body.style.overflow = "auto";
    document.getElementById('nav-bg').scrollTop = null;
  }

  render() {
    const images = [
      {
        original: require("url?mimetype=image/jpg!./screens/renins_standart.jpg"),
        thumbnail: require("url?mimetype=image/png!./screens/thumbnails/PostArticleWidget.png"),
      },
      {
        original: require("url?mimetype=image/jpg!./screens/lifehacker_aside.jpg"),
        thumbnail: require("url?mimetype=image/png!./screens/thumbnails/AsideWidget.png"),
      },
      {
        original: require("url?mimetype=image/jpg!./screens/trendspace_toster.jpg"),
        thumbnail: require("url?mimetype=image/png!./screens/thumbnails/TosterWidget.png"),
      },
      {
        original: require("url?mimetype=image/jpg!./screens/InteractiveInlineWidget.jpg"),
        thumbnail: require("url?mimetype=image/png!./screens/thumbnails/InteractiveWidget.png"),
      },
      {
        original: require("url?mimetype=image/jpg!./screens/inline.png"),
        thumbnail: require("url?mimetype=image/png!./screens/thumbnails/InlineWidget.png"),
      },
      {
        original: require("url?mimetype=image/jpg!./screens/pik_video.jpg"),
        thumbnail: require("url?mimetype=image/png!./screens/thumbnails/InlineVideoWidget.png"),
      }
    ]

    return (
      <ImageGallery
        ref={i => this._imageGallery = i}
        items={images}
        slideInterval={2000}
        onImageLoad={this.handleImageLoad}
    		showFullscreenButton={false}
    		showBullets={false}
    		showNav={false}
    		infinite={false}
    		showPlayButton={false}
        startIndex={this.props.slideNumber}
		  />
    );
  }
}

export default ContentSlider
