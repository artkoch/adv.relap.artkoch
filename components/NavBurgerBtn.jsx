import React, { Component } from 'react'


class BurgerBtn extends React.Component {
	constructor(props) {
		super(props);
	}

	handleClick() {
		this.props.expandNav();
	}

	render() {
		const changeToX = this.props.menuIsOpen ? "nav-menu-btn--open" : "";
		const layersAreWhite = this.props.atFifthScreen ? "menu-btn-layer--white" : "";
		return (
			<div className={"nav-menu-btn " + changeToX} onClick={this.handleClick.bind(this)}>
				<span className={"menu-btn-layer " + layersAreWhite}></span>
				<span className={"menu-btn-layer " + layersAreWhite}></span>
				<span className={"menu-btn-layer " + layersAreWhite}></span>
			</div>
		);
	}
}

export default BurgerBtn
