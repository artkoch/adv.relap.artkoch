import React, { Component } from 'react'


var Scroll = require('react-scroll');
var Link       = Scroll.Link;
class NavigationItems extends React.Component {
	render() {
		const showenClass = this.props.changeClass ? "" : "nav--hidden";
		const showItems = this.props.menuIsOpen ? "nav-items--showed" : "";
		const navLinkCyanClass = this.props.atFifthScreen ? "nav-link--FifthScreen" : "";
		return (
			<div className={"nav-items " + showItems}>
				<ul className="ul ul--nav ul--links">
					<li className="li li--nav li--links">
						<Link className={"nav-link " + navLinkCyanClass } activeClass="nav-link--active" to="secondScreen" smooth={true} spy={true} offset={-20} onClick={this.props.closeNavigation}>О проекте</Link>
					</li>
					<li className="li li--nav li--links">
						<Link className={"nav-link " + navLinkCyanClass } activeClass="nav-link--active" to="thirdScreen" smooth={true} spy={true} offset={-100} onClick={this.props.closeNavigation}>Форматы</Link>
					</li>
					<li className="li li--nav li--links">
						<Link className={"nav-link " + navLinkCyanClass } activeClass="nav-link--active" to="forthScreen" smooth={true} spy={true} offset={-100} onClick={this.props.closeNavigation}>Таргетинги</Link>
					</li>
					<li className="li li--nav li--links">
						<Link className={"nav-link " + navLinkCyanClass } activeClass="nav-link--active" to="fifthScreen" smooth={true} spy={true} offset={-50} onClick={this.props.closeNavigation}>Контакты</Link>
					</li>
				</ul>
			</div>
		);
	}
}

export default NavigationItems
