import React, { Component } from 'react'

import PdfIcon2 from 'babel!svg-react!./svg/Icon/PdfIcon2.svg?name=PdfIcon2';


class ActionButton extends React.Component {
	render(prop) {
		const showClass = this.props.menuIsOpen ? "visible" : "";

		 if (this.props.atFifthScreen) {
			return (
				<div className={"nav-download-btn " + showClass}>
					<PdfIcon2 className="nav-download-icon icons_1-18"/>
					<a className="nav-download-text" href="/Relap-2016.pdf">Презентация</a>
				</div>

			);
		} else {
			return (
        <a className={"nav-launch-btn " + showClass} href="#screen-fifth" onClick={this.props.closeNavigation}>
            <div>
                <span className="nav-launch-text">Оставить заявку</span>
            </div>
        </a>
			);
		}
	}
}

export default ActionButton
