import React, { Component } from 'react';
import ContentSlider from './ContentSlider.jsx';

import InlineVideoWidget from 'babel!svg-react!./svg/icon/InlineVideoWidget.svg?name=InlineVideoWidget';
import InlineWidget from 'babel!svg-react!./svg/icon/InlineWidget.svg?name=InlineWidget';
import PostArticleWidget from 'babel!svg-react!./svg/icon/PostArticleWidget.svg?name=PostArticleWidget';
import SideWidget from 'babel!svg-react!./svg/icon/SideWidget.svg?name=SideWidget';
import TosterWidget from 'babel!svg-react!./svg/icon/TosterWidget.svg?name=TosterWidget';
import InteractiveWidget from 'babel!svg-react!./svg/icon/InteractiveWidget.svg?name=InteractiveWidget';

import "../node_modules/react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from 'react-image-gallery';

class WidgetsViewer extends React.Component {
		render(props){
      const slideNumber = this.props.slideNumber;
			if (this.props.viewer) {
				return (
					<div className="WidgetsViewrWrapper">
							<div className="WidgetsViewr__image-container">
								<ContentSlider slideNumber={slideNumber} />
							</div>
							<div className="WidgetsViewr__bg" onClick={this.props.handleClick}></div>
							<div className="WidgetsViewr__close-icon" onClick={this.props.handleClick}>
								<div className="WidgetsViewr__ci-layer"></div>
								<div className="WidgetsViewr__ci-layer"></div>
							</div>
						</div>
				);

		} else {
				return null;
		}
	};
}






export default WidgetsViewer
