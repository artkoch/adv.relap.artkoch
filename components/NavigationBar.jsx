import React, { Component } from 'react'
import BurgerBtn from './NavBurgerBtn.jsx';
import ActionButton from './NavActionBtn.jsx';
import NavigationItems from './NavItems.jsx';



var Scroll = require('react-scroll');
var Link       = Scroll.Link;
import RelapLogoType from 'babel!svg-react!./svg/RelapLogoType.svg?name=RelapLogoType';

function getFirstScreenHeight() {
	return document.getElementById('screen-first').offsetHeight;
}

function getScreenPosFromTop(screen) {
	return document.getElementById(screen).offsetTop;
}

function getScreenHeight(screen) {
	return document.getElementById(screen).offsetHeight;
}

class NavigationBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			menuIsOpen: false,
			showen: false,
			atFifthScreen: false
		};
		this.closeNav = this.closeNav.bind(this);
		this.openNav = this.openNav.bind(this);
		this.switchColor = this.switchColor.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
		this.expandNav = this.expandNav.bind(this);
	}

	componentDidMount() {
		document.addEventListener('scroll', this.handleScroll);
	}

	componentWillUnmount() {
		document.addEventListener('scroll', this.handleScroll);
	}

	handleScroll(e) {
		if (scrollY > getFirstScreenHeight() - 80) {
			this.setState({showen: true});
		} else {
			this.setState({showen: false});
		}

		if (scrollY > getScreenPosFromTop('screen-fifth') - 60) {
			this.setState({atFifthScreen: true});
		} else {
			this.setState({atFifthScreen: false});
		}
        this.switchColor();
    }

	switchColor() {
		document.getElementById('nav').style.background = this.state.atFifthScreen ? "#33c4cd" : "white";
		document.getElementById('nav').style.boxShadow = this.state.atFifthScreen ? "none" : "0px 1px 0px 0px rgba(0,0,0,0.2)";
		document.getElementById('nav-logo-icon').style.fill = this.state.atFifthScreen ? "white" : "#33c4cd";
	}

	closeNav() {
		this.setState({
			navBgShow: ""
		});
		document.body.style.overflow = null;
	}
    disableScroll() {
        document.body.style.overflow = "hidden";
		document.getElementById('nav-bg').scrollTop = 0;
    }
	openNav() {
		this.setState({
			navBgShow: "nav-bg--visible"
		});
		this.disableScroll();
	}

	closeNavigation() {
        if (this.state.menuIsOpen) {
			this.setState({menuIsOpen: false});
			this.closeNav();
		}
    }
	expandNav() {
		if (this.state.menuIsOpen) {
			this.setState({menuIsOpen: false});
			this.closeNav();
		} else {
			this.setState({menuIsOpen: true});
			this.openNav();
		}
	}


	render() {
		const showenClass = this.state.showen ? "" : "nav--hidden";
		return (
			<div className="nav-wrapper">
				<div className={"nav " + showenClass} id="nav">
					<div className="nav-logo">
						<a href="/" ><RelapLogoType className="icon nav-logo-icon icon-relap-logo" id="nav-logo-icon"/></a>
					</div>
            <BurgerBtn expandNav={this.expandNav} menuIsOpen={this.state.menuIsOpen} atFifthScreen={this.state.atFifthScreen}/>
						<NavigationItems menuIsOpen={this.state.menuIsOpen} atFifthScreen={this.state.atFifthScreen} closeNavigation={this.closeNavigation.bind(this)}/>
            <ActionButton menuIsOpen={this.state.menuIsOpen} atFifthScreen={this.state.atFifthScreen} closeNavigation={this.closeNavigation.bind(this)}/>
				</div>
				<div className={"nav-bg " + this.state.navBgShow} id="nav-bg" onClick={this.expandNav}></div>
			</div>
		);
	}
}

export default NavigationBar
