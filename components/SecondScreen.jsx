var React = require('react');


import IBM from 'babel!svg-react!./svg/brands/IBM.svg?name=IBM';
import AudiBlack from 'babel!svg-react!./svg/brands/AudiBlack.svg?name=AudiBlack';
import RaiffeisenBank from 'babel!svg-react!./svg/brands/RaiffeisenBank.svg?name=RaiffeisenBank';
import Bork from 'babel!svg-react!./svg/brands/Bork.svg?name=Bork';
import CarPrice from 'babel!svg-react!./svg/brands/CarPrice.svg?name=CarPrice';
import GetTaxi from 'babel!svg-react!./svg/brands/GetTaxi.svg?name=GetTaxi';
import Intel from 'babel!svg-react!./svg/brands/Intel.svg?name=Intel';
import Lenovo from 'babel!svg-react!./svg/brands/Lenovo.svg?name=Lenovo';
import Panasonic from 'babel!svg-react!./svg/brands/Panasonic.svg';
import RosTelecom from 'babel!svg-react!./svg/brands/RosTelecom.svg?name=RosTelecom';
import Samsung from 'babel!svg-react!./svg/brands/Samsung.svg?name=Samsung';
import Tele2 from 'babel!svg-react!./svg/brands/Tele2.svg?name=Tele2';
import Tinkoff from 'babel!svg-react!./svg/brands/Tinkoff.svg?name=Tinkoff';
import Toyota from 'babel!svg-react!./svg/brands/Toyota.svg?name=Toyota';
import TNT from 'babel!svg-react!./svg/brands/TNT2.svg?name=TNT';

class SecondScreen extends React.Component {
	render(){

		return (
			<div className="screen screen-second" id="screen-second">
				<div className="screen__container-one">
					<h3 className="h3">Реклама с Relap.io — это огромная аудитория</h3>
					<div className="factoids">
						<div className="factoid">
							<span className="factoid__text">более</span>
							<span className="factoid__batch--addition">3 млрд</span>
							<span className="factoid__text">рекламных показов</span>
						</div>
						<div className="factoid">
							<span className="factoid__text">более</span>
							<span className="factoid__batch">100 млн</span>
							<span className="factoid__text">уникальных пользователей</span>
						</div>
						<div className="factoid">
							<span className="factoid__text">более</span>
							<span className="factoid__batch">3000</span>
							<span className="factoid__text">сайтов партнеров</span>
						</div>
					</div>
				</div>
				<div className="screen__container-two">
					<p className="p">Рекламодатели, которые используют технологии Relap.io:</p>
					<div className="brands-wrapper">
            <div className="brands">
                <div className="brands__row brands__row--first">
                    <IBM className="icon brands__logo icon-IBM" />
                    <RaiffeisenBank className="icon brands__logo icon-Raiffeisen_Bank" />
                    <Bork className="icon brands__logo icon-bork"/>
                    <Lenovo className="icon brands__logo icon-lenovo"/>
                    <CarPrice className="icon brands__logo icon-carprice"/>
                </div>
                <div className="brands__row">
                    <Intel className="icon brands__logo icon-Intel"/>
                    <RosTelecom className="icon brands__logo icon-rostelecom"/>
                    <Panasonic className="icon brands__logo icon-Panasonic-Logo"/>
                    <AudiBlack className="icon brands__logo icon-audi_black"/>
                    <GetTaxi className="icon brands__logo icon-gettaxi"/>
                </div>
                <div className="brands__row">
                    <Samsung className="icon brands__logo icon-samsung"/>
                    <Tinkoff className="icon brands__logo icon-tinkoff_simple"/>
                    <Tele2 className="icon brands__logo icon-Tele2"/>
                    <Toyota className="icon brands__logo icon-toyota"/>
                    <TNT className="icon brands__logo icon-toTNTyota"/>
                </div>
            </div>
            <p className="p p--more-partners">и ещё более 400 партнеров</p>
					</div>


					<div className="brands-two-columns">
						<div className="brands__row brands__row--two-columns brands__row--tc-first">
							<IBM className="icon brands__logo icon-IBM" />
							<RaiffeisenBank className="icon brands__logo icon-Raiffeisen_Bank" />
							<Bork className="icon brands__logo icon-bork"/>
							<Lenovo className="icon brands__logo icon-lenovo"/>
							<CarPrice className="icon brands__logo icon-carprice"/>
							<Intel className="icon brands__logo icon-Intel"/>
							<RosTelecom className="icon brands__logo icon-rostelecom"/>
							<Panasonic className="icon brands__logo icon-Panasonic-Logo"/>
						</div>
						<div className="brands__row brands__row--two-columns">
							<AudiBlack className="icon brands__logo icon-audi_black"/>
							<GetTaxi className="icon brands__logo icon-gettaxi"/>
							<Samsung className="icon brands__logo icon-samsung"/>
							<Tinkoff className="icon brands__logo icon-tinkoff_simple"/>
							<Tele2 className="icon brands__logo icon-Tele2"/>
							<Toyota className="icon brands__logo icon-toyota"/>
							<TNT className="icon brands__logo icon-toTNTyota"/>
							<p className="p p--more-partners">и ещё более 400 партнеров</p>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default SecondScreen
