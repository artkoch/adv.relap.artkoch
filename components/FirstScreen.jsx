import React, { Component } from 'react';

import RelapLogoType from 'babel!svg-react!./svg/RelapLogoType.svg?name=RelapLogoType';
import Magnet from 'babel!svg-react!./svg/Icon/Magnet.svg?name=Magnet';

var Scroll = require('react-scroll');
var Link       = Scroll.Link;

function clickLog(e) {
		e.preventDefault();
		console.log('clicked');
}

function getScreenPosFromTop(screen) {
	return document.getElementById(screen).offsetTop;
}

class ButtonNormal extends React.Component {

	render(props) {
		return <button className="button-simple" >{this.props.name}</button>;
	}
}

class ButtonFilled extends Component {
	render(props) {
		return (
			<button className="button-simple button-simple--fill" onClick={clickLog}>{this.props.name}</button>
		);
	}
}

class FirstScreen extends Component {
	render(){
		return (
			<div className="screen screen-first" id="screen-first">
				<RelapLogoType  className="icon icon-relap-logo"/>
				<h1 className="h1">Рекламные возможности <br/>Relap.io</h1>
				<div className="flex-container">
					<div className="buttons-wrapper">
							<Link to="secondScreen" smooth={true} offset={-10}><ButtonNormal name="Подробнее"/></Link>
							<Link to="fifthScreen" smooth={true} offset={-50}><ButtonFilled name="Оставить заявку"/></Link>
					</div>
					<Magnet className="icon icon-magnet" />
				</div>
			</div>
		)
	}
}

export default FirstScreen
