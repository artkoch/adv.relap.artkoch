import Validation from 'react-validation';
import React, {Component, PropTypes} from 'react';
// import validator from 'validator';


export default class ContactFormInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: ""
    };
    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.whatsInTheInput = this.whatsInTheInput.bind(this);
  }

  onFocus() {
		this.props.updateInputState('small');
	}

  onBlur() {
		if (this.state.inputValue == "") {
  		this.props.updateInputState();
		} else {
			this.props.updateInputState('small');
		}
	}

  whatsInTheInput(e) {
		this.setState({
			inputValue: e.target.value
		});
	}

    render() {
      return <Validation.components.Input
              className="contactus__input"
              onChange={this.whatsInTheInput}
              onFocus={this.onFocus}
              onBlur={this.onBlur}
              value='artkoch@artkoch.ru'
              name='email'
              validations={['required', 'email']}
              />;
    }
}
