import React, { Component } from 'react';

import WidgetsViewer from "./WidgetsViewer.jsx";

import FingerRipples from 'babel!svg-react!./svg/icon/FingerRipples.svg?name=FingerRipples';
import InlineVideoWidget from 'babel!svg-react!./svg/icon/InlineVideoWidget.svg?name=InlineVideoWidget';
import InlineWidget from 'babel!svg-react!./svg/icon/InlineWidget.svg?name=InlineWidget';
import PostArticleWidget from 'babel!svg-react!./svg/icon/PostArticleWidget.svg?name=PostArticleWidget';
import SideWidget from 'babel!svg-react!./svg/icon/SideWidget.svg?name=SideWidget';
import TosterWidget from 'babel!svg-react!./svg/icon/TosterWidget.svg?name=TosterWidget';
import InteractiveWidget from 'babel!svg-react!./svg/icon/InteractiveWidget.svg?name=InteractiveWidget';



class ThirdScreen extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			viewer: false,
			slideNumber: '0'
		};
		this.handleClick = this.handleClick.bind(this);
		this.handleClickedW = this.handleClickedW.bind(this);
	}

	handleClick() {
		this.state.viewer ? this.setState({viewer: false}) : this.setState({viewer: true});
	}

	handleClickedW(slideNumber) {
		this.setState({viewer: true, slideNumber: slideNumber });
	}
// clickedWidget={this.state.clickedWidget}
	render(){
		return (
			<div className="screen screen-third" id="screen-third">
				<div className="screen__container-one"><h3 className="h3 h3--third-screen">Реклама в&nbsp;формате, в&nbsp;котором пол зовател прив к&nbsp;получат информаци</h3>
					<ul className="ul ul--third-screen">
						<li className="li li--third-screen">Мы&nbsp;покажем вашу рекламу в&nbsp;рекомендательном блоке среди персонально подобранных для пользователя материалов.</li>
						<li className="li li--third-screen">В&nbsp;этом блоке пользователь привык находить новую информацию.</li>
						<li className="li li--third-screen">Реклама органично встраивается в&nbsp;статью и&nbsp;дополняет ее&nbsp;содержание.</li>
						<li className="li li--third-screen">Реклама остается заметной, но&nbsp;не&nbsp;выглядит навязчиво и&nbsp;не&nbsp;разрушает&nbsp;UX.</li>
					</ul>
					<WidgetsViewer viewer={this.state.viewer} slideNumber={this.state.slideNumber} handleClick={this.handleClick}  />
					<FingerRipples className="icon-p-bullet icon-1-11"/>
					<p className="p p--icon-before p--text-transitions">С&nbsp;рекламного блока пользователь переходит к&nbsp;вам на&nbsp;сайт, спецпроект, статью о&nbsp;вас или в&nbsp;блог вашей компании.</p>
				</div>
				<div className="screen__container-two">
					<p className="p widgets__headline">Рекомендации вокруг статьи</p>
					<div className="widgets__wrapper">
						<div className="widgets__card" onClick={() => this.handleClickedW(0)}>
							<PostArticleWidget className="widgets__icon icons_1-13"/>
							<p className="p widgets__type">в конце статьи</p>
						</div>
						<div className="widgets__card" onClick={() => this.handleClickedW(1)}>
							<SideWidget className="widgets__icon icons_1-12" />
							<p className="p widgets__type">сбоку</p>
						</div>
						<div className="widgets__card" onClick={() => this.handleClickedW(2)}>
							<TosterWidget className="widgets__icon icons_1-14" />
							<p className="p widgets__type">в тостере</p>
						</div>
					</div>
					<p className="p widgets__headline">Внутри статьи</p>
					<div className="widgets__wrapper">
						<div className="widgets__card" onClick={() => this.handleClickedW(3)}>
							<InlineWidget className="widgets__icon icons_1-17" />
							<p className="p widgets__type">инлайн</p>
						</div>
						<div className="widgets__card widgets__card--two-line-name" onClick={() => this.handleClickedW(4)}>
							<InteractiveWidget className="widgets__icon icons_1-16" />
							<p className="p widgets__type">интерактивный инлайн</p>
						</div>
						<div className="widgets__card" onClick={() => this.handleClickedW(5)}>
							<InlineVideoWidget className="widgets__icon icons_1-15"/>
							<p className="p widgets__type">видео</p>
						</div>
					</div>

				</div>
			</div>
		)
	}
}

export default ThirdScreen
