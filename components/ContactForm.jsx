import React, {Component, PropTypes} from 'react';
import Validation from 'react-validation';
import ContactFormInput from './ContactFormInput.jsx';
import ContactFormTextarea from './ContactFormTextarea.jsx';
// import validator from 'validator';

function sendForm(e) {
  e.preventDefault();
  fetch('https://learn.javascript.ru/article/ajax-xmlhttprequest/phones/phones.json',
  {
    method: "get",
    body: new FormData(document.getElementsByClassName('contactus')[0])
  }).then(function(response) {
    return console.log('Got it!');
  }).catch(function(err) {
    // Error :(
    return console.log('Aaa, lost it :(');
  });
}

export default class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emailLabelSmall: false,
      textareaLabelSmall: false,
    };
    this.updateInputState = this.updateInputState.bind(this);
    this.updateTextareaState = this.updateTextareaState.bind(this);
    this.sendForm = this.sendForm.bind(this);
  }

  sendForm(e) {
    e.preventDefault();
    fetch('https://learn.javascript.ru/article/ajax-xmlhttprequest/phones/phones.json',
    {
      method: "get",
      body: new FormData(document.getElementsByClassName('contactus')[0])
    }).then(function(response) {
      console.log('Got it!');
    }).catch(function(err) {
      console.log('Aarr, lost it :(');
    });
  }

  ComponentDidMount() {
    sendForm();
  }

  updateInputState(p) {
    if (p) {
        this.setState({emailLabelSmall: true})
        console.log('updateInputState ture? ' + this.state.emailLabelSmall);
    } else {
        this.setState({emailLabelSmall: false})
        console.log('updateInputState false? ' + this.state.emailLabelSmall);
    }
  }

  updateTextareaState(p) {
    if (p) {
        this.setState({textareaLabelSmall: true})
        console.log('textareaLabelSmall ture? ' + this.state.textareaLabelSmall);
    } else {
        this.setState({textareaLabelSmall: false})
        console.log('textareaLabelSmall false? ' + this.state.textareaLabelSmall);
    }
  }
    render() {
      const emailLabelSmall = this.state.emailLabelSmall ? "contactus__input-placholder--small" : "";
      const textareaLabelSmall = this.state.textareaLabelSmall ? "contactus__input-placholder--small" : "";
      return  <div className="screen-fifth__contactus">
                <Validation.components.Form className="contactus" onSubmit={this.sendForm}>
                    <div className="contactus__input-wrapper">
                        <label className={"contactus__input-placholder contactus__email " + emailLabelSmall}>
                          Электронная почта
                        </label>
                        <ContactFormInput  emailLabelSmall={this.state.emailLabelSmall} updateInputState={this.updateInputState} />
                    </div>
                    <div className="contactus__textarea-wrapper">
                        <label className={"contactus__input-placholder contactus__message " + textareaLabelSmall} >
                          Текст заявки
                        </label>
                        <ContactFormTextarea value={'Hello!'} textareaLabelSmall={this.state.textareaLabelSmall} updateTextareaState={this.updateTextareaState} />
                    </div>
                    <div>
                        <Validation.components.Button
                          className="contactus__submit"
                          type="submit"
                           >
                            Отправить заявку
                        </Validation.components.Button>
                    </div>
                </Validation.components.Form>
              </div>;
    }
}
