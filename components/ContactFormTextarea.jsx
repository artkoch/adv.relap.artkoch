import Validation from 'react-validation';
import React, {Component, PropTypes} from 'react';
// import validator from 'validator';


export default class ContactFormTextarea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textareaValue: "",
      textareaHeight: 60
    };
    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.changeTextareaHeight = this.changeTextareaHeight.bind(this);
  }

  onFocus() {
		this.props.updateTextareaState('small');
	}

  onBlur() {
		if (this.state.textareaValue == "") {
  		this.props.updateTextareaState();
		} else {
			this.props.updateTextareaState('small');
		}
	}

  changeTextareaHeight(e) {
    this.setState({textareaValue: e.target.value});
      if (this.state.textareaValue.length > 20 && this.state.textareaValue.length % 22 == 0) {
        this.setState({textareaHeight: this.state.textareaHeight + 20});
      } else {
        console.log('not 22 but ' + this.state.textareaValue.length);
      }
  }

  render() {
    const textareaHeight = {height: this.state.textareaHeight + 'px'};
    return <Validation.components.Textarea
            style={textareaHeight}
            className="contactus__textarea"
            onChange={this.changeTextareaHeight}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            value='Hello!'
            name='message'
            validations={['required']}
            />;
  }
}
